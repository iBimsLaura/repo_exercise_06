﻿using UnityEngine;
using System.Collections;

namespace CoAHomework
{   //not working
    public class NumberChecker : MonoBehaviour
    {
        public int[] numbers = { 1, 2, 3, 4, 5 };
        public int numberToSkip = 3;
        public int numberToBreakout = 2;
      
        void Start()
        {
            void CountingNumbers()
            {
                for(int i = 0; i <= numbers.Length; i++)
                {
                    if(i == 3)
                    {
                        Debug.Log("Skipped 3");
                    }

                    else
                    {
                        Debug.Log("Current value of the array is: " + i);
                    }

                    if(i == 5)
                    {
                        Debug.Log("The Loop is finished");
                    }
                }

                void SomethingAboutNumbers(int i = 2)
                {
                    for (i = 2; i <= numbers.Length; i++) 
                    {
                        if(i == numbers.Length)
                        {
                            Debug.Log("I'm skipping.");
                        }

                        else
                        {
                            Debug.Log("The current value is: " + i);
                        }

                        if(i == numbers.Length)
                        {
                            Debug.Log("Loop has been finished (successfully)");
                        }
                    }
                }

                void WontWorkEither(int i = 0)
                {
                    for (i = 0; i <= 2; i++) 
                    {
                        if (i == numbers.Length)
                        {
                            Debug.Log("Skipping");
                        }

                        else
                        {
                            Debug.Log("The current value is: " + i);
                        }

                        if (i == 2)
                        {
                            Debug.Log("I'm breaking out from the Loop");
                            Debug.Log("Loop is finished");
                        }
                    }
                }
            }
        }
    }
}

        

